package co.id.dasa.prodiacounterapp;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web/")
public class WebController {
 
    @GetMapping("/")
    public String index(HttpServletRequest request,Model model){
        
        model.addAttribute("namaDariBackend", "Belum Ada Judul");
        model.addAttribute("namaDariBackend2", "Sudah Ada Judul");
        return "/index";
    }
}
