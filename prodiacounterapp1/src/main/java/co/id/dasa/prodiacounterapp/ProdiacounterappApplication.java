package co.id.dasa.prodiacounterapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@SpringBootApplication
@EnableDiscoveryClient
public class ProdiacounterappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdiacounterappApplication.class, args);
		System.out.println("Hello World Springboot !!!");
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	

}
