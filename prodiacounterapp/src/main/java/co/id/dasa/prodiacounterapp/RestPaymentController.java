package co.id.dasa.prodiacounterapp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

@Controller //agar springboot nge scan class ini sebagai routing
@RequestMapping("/api/payment/v1")
public class RestPaymentController {

@GetMapping("/getName")
public @ResponseBody List<String> getName(){
        //logic database 
        //logic mvc 
        List <String> listNama = new ArrayList<String>();

        listNama.add("payment bruce");
        listNama.add("payment wilis");
        listNama.add("payment cakeep");

    return listNama;
 }

 @PostMapping("/setName")
 public @ResponseBody String addName(@RequestParam String 
 fname,@RequestParam String lname){
        //logic database 
        //logic mvc 
    return fname+" "+lname;
 }

 @PostMapping("/cekName")
 public @ResponseBody String cekName(@RequestParam String 
 fname,@RequestParam String lname){
        
    if(fname.equals(lname)){
        return "First name dan last name tidak boleh sama !!";
    }

    return fname+" "+lname;
 }
    
}
