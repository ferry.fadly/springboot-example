package co.id.dasa.prodiacounterapp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //agar springboot nge scan class ini sebagai routing
@RequestMapping("/api/v1")
public class RestController {

@GetMapping("/getName")
public @ResponseBody List<String> getName(){
        //logic database 
        //logic mvc 
        List listNama = new ArrayList();

        listNama.add("bruce");
        listNama.add("wilis");
        listNama.add("cakeep");

    return listNama;
 }

 @PostMapping("/setName")
 public @ResponseBody String addName(@RequestParam String fname,@RequestParam String lname){
        //logic database 
        //logic mvc 
    return fname+" "+lname;
 }
    
}
