package test.springboot.rest;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.Map;

@Controller
@RequestMapping("/api/v1")
public class RestController{

    @GetMapping("/getName")
    public  @ResponseBody  String getName( ) {
    
        return "Bruce";
    }

    
    @PostMapping("/add")
    public  @ResponseBody  String add( @RequestParam Map<String,String> allParams ) {
        String fname = "";
        String lname = "";

        if(allParams.containsKey("fname")) {
        	fname = allParams.get("fname");
        }

        if(allParams.containsKey("lname")) {
        	lname = allParams.get("lname");
        }

        return fname+" "+lname;
    }

}