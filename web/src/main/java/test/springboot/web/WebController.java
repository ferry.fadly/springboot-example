package test.springboot.web;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import java.util.Map;

@Controller
public class WebController{

    @GetMapping("/")
    public String cashRequestList(HttpServletRequest request) {

        return "/web/index";
    }

}